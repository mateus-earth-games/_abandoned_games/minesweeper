//----------------------------------------------------------------------------//
// Status Hud                                                                 //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
const STATUS_HUD_BACKGROUND_COLOR = chroma("397A21");

//------------------------------------------------------------------------------
class StatusHud
    extends PIXI.Container
{
    //--------------------------------------------------------------------------
    constructor(size)
    {
        super();

        //
        // iVars
        // Properties
        this.size = size;
        this.graphics = new PIXI.Graphics();

        this.flagIcon = Sprite_Create("flag_icon");
        this.flagText = this._CreateText("999");

        this.clockIcon = Sprite_Create("clock_icon");
        this.clockText = this._CreateText("999");

        //
        // Initialize.
        this._DrawGraphics();
        this._CalculateUI ();

        this.SetFlagsCount(0);
        this.SetTimeCount (0);

        this.addChild(this.graphics);

        this.addChild(this.flagIcon);
        this.addChild(this.flagText);

        this.addChild(this.clockIcon);
        this.addChild(this.clockText);
    } // ctor

    //--------------------------------------------------------------------------
    SetFlagsCount(c)
    {
        this.flagText.text = c;
    } // SetFlagsCount

    //--------------------------------------------------------------------------
    SetTimeCount(c)
    {
        this.clockText.text = c;
    } // SetTimeCount


    //--------------------------------------------------------------------------
    _CreateText()
    {
        const style = new PIXI.TextStyle({
            fontFamily    : "Arial",
            fontSize      : 20,
            fontWeight    : "bold",
            fill          : 0xffffff,
        });

        let text = new PIXI.Text("", style);
        return text;
    }

    //--------------------------------------------------------------------------
    _DrawGraphics()
    {
        this.graphics.beginFill(STATUS_HUD_BACKGROUND_COLOR.num(), 1)
        this.graphics.drawRect(0,0, this.size.x, this.size.y);
        this.graphics.endFill();
    }

    //--------------------------------------------------------------------------
    _CalculateUI()
    {
        const center_x = this.size.x / 2;
        const center_y = this.size.y / 2;
        const GAP        = 30;
        const INNER_GAP  = 5;

        this.flagText.anchor.set(0, 0.5);
        this.flagText.x = (center_x - this.flagText.width - GAP);
        this.flagText.y = (center_y);

        this.flagIcon.anchor.set(1, 0.5);
        this.flagIcon.x = this.flagText.x - INNER_GAP;
        this.flagIcon.y = this.flagText.y;

        this.clockIcon.anchor.set(0, 0.5);
        this.clockIcon.x = (center_x + GAP);
        this.clockIcon.y = (center_y);

        this.clockText.anchor.set(0, 0.5);
        this.clockText.x = this.clockIcon.x + this.clockIcon.width + INNER_GAP;
        this.clockText.y = this.clockIcon.y;
    } // SetTimeCount
}; // class StatusHud
