//----------------------------------------------------------------------------//
//                       __      __                  __   __                  //
//               .-----.|  |_.--|  |.--------.---.-.|  |_|  |_                //
//               |__ --||   _|  _  ||        |  _  ||   _|   _|               //
//               |_____||____|_____||__|__|__|___._||____|____|               //
//                                                                            //
//  File      : Particles.js                                                  //
//  Project   : minesweeper                                                   //
//  Date      : Sep 13, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//----------------------------------------------------------------------------//




//----------------------------------------------------------------------------//
// Bomb Particle                                                              //
//----------------------------------------------------------------------------//
const BOMB_PARTICLE_ANIM_OPEN_TIME = 3;

const BOMB_PARTICLE_ANIM_FORCE_MIN_X = -1;
const BOMB_PARTICLE_ANIM_FORCE_MAX_X = +1;

const BOMB_PARTICLE_ANIM_FORCE_MIN_Y = -10;
const BOMB_PARTICLE_ANIM_FORCE_MAX_Y = -15;

const BOMB_PARTICLE_ANIM_ANGLE_MIN = 0; //-MATH_PI;
const BOMB_PARTICLE_ANIM_ANGLE_MAX = 0; //+MATH_PI;

//------------------------------------------------------------------------------
class BombParticle
{
    //--------------------------------------------------------------------------
    constructor(position, size, color)
    {
        this.position = Vector_Create(position.x, position.y);
        this.size     = Vector_Create(size.x, size.y);
        this.color    = color;
        this.rotation = 0;

        this.isAnimating = false;
        this.isDone      = false;

        this.animationTimer        = new Base_Timer(BOMB_PARTICLE_ANIM_OPEN_TIME);
        this.openAnimationForce    = this._CalcRandomOpenForce   ();
        this.openAnimationAngle    = this._CalcOpenAnimationAngle();
        this.openAnimationVelocity = Vector_Create(0, 0);
        console.log(this);
        // debugger;
    } // ctor

    //--------------------------------------------------------------------------
    StartOpenAnimation()
    {
        if(this.isAnimating) {
            return;
        }

        this.isAnimating = true;
        this.animationTimer.Start();
    } // StartOpenAnimation

    //--------------------------------------------------------------------------
    Update(dt)
    {
        if(!this.isAnimating || this.isDone) {
            return;
        }

        this.animationTimer.Update(dt);
        if(this.isAnimating) {
            this.openAnimationForce.x *= 0.1; // decay...
            this.openAnimationForce.y *= 0.9; // decay...
            let accY = 7.6 + this.openAnimationForce.y ;

            this.openAnimationVelocity.x += this.openAnimationForce.x;
            this.openAnimationVelocity.y += accY;
            if(this.openAnimationVelocity.y >= 1) {
                this.openAnimationVelocity.y = 1;
            }

            this.position.x += this.openAnimationVelocity.x * dt;
            this.position.y += this.openAnimationVelocity.y * dt;
            this.rotation   += this.openAnimationAngle      * dt;

            if(this.animationTimer.isDone) {
                this.isAnimating = false;
                this.isDone      = true;
            }
        }
    } // Update

    //--------------------------------------------------------------------------
    draw()
    {
        Canvas_Push();
            Canvas_Translate(this.position.x* 80, this.position.y);

            if(this.isAnimating) {
                Canvas_Scale(1 - this.animationTimer.ratio / 2);
                Canvas_Rotate(this.rotation);
            }

            let x = Math_Sin(this.animationTimer.ratio * MATH_2PI)     * 90;
            let y = Math_Sin(this.animationTimer.ratio * MATH_PI / 2)  * 30;

            Canvas_SetFillStyle(this.color);
            Canvas_FillRect(x, y, this.size.x / 2, this.size.y / 4);
        Canvas_Pop();
    }

     //--------------------------------------------------------------------------
     _CalcRandomOpenForce()
     {
         let x = Math_Random(
             BOMB_PARTICLE_ANIM_FORCE_MIN_X,
             BOMB_PARTICLE_ANIM_FORCE_MAX_X
         );
         let y = Random_Int(
             BOMB_PARTICLE_ANIM_FORCE_MIN_Y,
             BOMB_PARTICLE_ANIM_FORCE_MAX_Y
         );

         return Vector_Create(x, y);
     } // _CalcRandomOpenForce

      //--------------------------------------------------------------------------
      _CalcOpenAnimationAngle()
      {
          return Math_Map(
              this.openAnimationForce.x,
              BOMB_PARTICLE_ANIM_FORCE_MIN_X,
              BOMB_PARTICLE_ANIM_FORCE_MAX_X,
              BOMB_PARTICLE_ANIM_ANGLE_MIN,
              BOMB_PARTICLE_ANIM_ANGLE_MAX
          );
      } // _CalcOpenAnimationAngle
}; // class BombParticle
