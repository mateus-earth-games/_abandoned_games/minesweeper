//----------------------------------------------------------------------------//
// Palette                                                                    //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
class Palette
{
    //--------------------------------------------------------------------------
    constructor()
    {
        this.blockClosedColorLight = chroma("aad751");
        this.blockClosedColorDark  = chroma("a2d149");

        this.blockOpenedColorLight = chroma("EAC299");
        this.blockOpenedColorDark  = chroma("DBB894");

        this.blockEdgeColor        = chroma("#87af3a");
        this.blockHighlightedColor = this.blockClosedColorLight.brighten(1);

        this.blockValueColors = [
            chroma("#006ED9"), // 1
            chroma("#009533"), // 2
            chroma("#E3000C"), // 3
            chroma("#8800A7"), // 4
            chroma("#FF8B00"), // 5
            chroma("#000000"), // 6
            chroma("#FF00FF"), // 7
            chroma("#FFFFFF"), // 8
        ];

    } // ctor

    //--------------------------------------------------------------------------
    GetBlockClosedColor(isLight)
    {
        if(isLight) {
            return this.blockClosedColorLight;
        } else {
            return this.blockClosedColorDark;
        }
    } // GetBlockClosedColor

    //--------------------------------------------------------------------------
    GetBlockOpenedColor(isLight)
    {
        if(isLight) {
            return this.blockOpenedColorLight;
        } else {
            return this.blockOpenedColorDark;
        }
    } // getBlockOpenColor

    //--------------------------------------------------------------------------
    getBlockHighlightedColor()
    {
        return this.blockHighlightedColor;
    } // getBlockHighlightedColor

    //--------------------------------------------------------------------------
    getBlockValueColor(value)
    {
        return this.blockValueColors[value -1];
    } // getBlockValueColor

    //--------------------------------------------------------------------------
    GetBlockEdgeColor()
    {
        return this.blockEdgeColor;
    } // GetBlockEdgeColor

    //--------------------------------------------------------------------------
    GetRandomBombColor()
    {
        return chroma.random();
    } // GetRandomBombColor

}; // class Palette
