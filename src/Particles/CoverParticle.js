//----------------------------------------------------------------------------//
// Cover Particle                                                             //
//----------------------------------------------------------------------------//
const COVER_PARTICLE_ANIM_OPEN_TIME = 1.2;

const COVER_PARTICLE_ANIM_FORCE_MIN_X = -2;
const COVER_PARTICLE_ANIM_FORCE_MAX_X = +2;

const COVER_PARTICLE_ANIM_FORCE_MIN_Y = -90;
const COVER_PARTICLE_ANIM_FORCE_MAX_Y = -90;

const COVER_PARTICLE_ANIM_ANGLE_MIN = -MATH_PI;
const COVER_PARTICLE_ANIM_ANGLE_MAX = +MATH_PI;

const COVER_PARTICLE_EDGE_INDEX_TOP    = 0;
const COVER_PARTICLE_EDGE_INDEX_RIGHT  = 1;
const COVER_PARTICLE_EDGE_INDEX_BOTTOM = 2;
const COVER_PARTICLE_EDGE_INDEX_LEFT   = 3;

const COVER_PARTICLE_EDGE_SIZE = 2;

//------------------------------------------------------------------------------
class CoverParticle
    extends PIXI.Sprite
{
    //--------------------------------------------------------------------------
    constructor(width, height, color)
    {
        super(PIXI.Texture.WHITE);

        //
        // iVars
        // Properties
        this.rotation = 0;

        this.isAnimating = false;
        this.isDone      = false;

        this.animationTimer        = new Base_Timer(COVER_PARTICLE_ANIM_OPEN_TIME);
        this.openAnimationForce    = this._CalcRandomOpenForce   ();
        this.openAnimationAngle    = this._CalcOpenAnimationAngle();
        this.openAnimationVelocity = Vector_Create(0, 0);

        this.borderEdges = [false, false, false, false];

        this.tint     = color.num();
        this.graphics = new PIXI.Graphics();

        //
        // Initialize.
        this._DrawGraphics();
        this.addChild(this.graphics);

        this.width       = width;
        this.height      = height;
        this.targetScale = this.scale.x;
    } // ctor

    //--------------------------------------------------------------------------
    StartOpenAnimation()
    {
        if(this.isAnimating) {
            return;
        }

        this.isAnimating = true;
        this.animationTimer.Start();

        this.removeChild(this.graphics);
        this.graphics.destroy();
        this.graphics = null;
    } // StartOpenAnimation

    //--------------------------------------------------------------------------
    OpenEdge(v)
    {
        if(v.y < 0) {
            this.borderEdges[COVER_PARTICLE_EDGE_INDEX_TOP] = true;
        } else if(v.y > 0) {
            this.borderEdges[COVER_PARTICLE_EDGE_INDEX_BOTTOM] = true;
        } else if(v.x < 0) {
            this.borderEdges[COVER_PARTICLE_EDGE_INDEX_LEFT] = true;
        } else if(v.x > 0) {
            this.borderEdges[COVER_PARTICLE_EDGE_INDEX_RIGHT] = true;
        }

        this._DrawGraphics();
    }

    //--------------------------------------------------------------------------
    Update(dt)
    {
        if(!this.isAnimating || this.isDone) {
            return;
        }

        this.animationTimer.Update(dt);
        if(this.isAnimating) {
            this.openAnimationForce.y *= 0.7; // decay...
            let acc = 10 + this.openAnimationForce.y;

            this.openAnimationVelocity.x += this.openAnimationForce.x;
            this.openAnimationVelocity.y += acc;

            this.x        += this.openAnimationVelocity.x * dt;
            this.y        += this.openAnimationVelocity.y * dt;
            this.rotation += this.openAnimationAngle      * dt;

            const scale = this.targetScale * (1 - this.animationTimer.ratio);
            this.scale.set(scale);

            if(this.animationTimer.isDone) {
                this.isAnimating = false;
                this.isDone      = true;
            }
        }
    } // Update

    //--------------------------------------------------------------------------
    _DrawGraphics()
    {
        // @notice(stdmatt): The graphics REALLY don't behave in the way that
        // I was expecting... If i scale the container and draw the lines
        // all of them get really really out of the correct position.
        //
        // So the trick is to scale back the Particle, draw the graphics
        // then scale it to the "correct" size...
        this.scale.set(1);

        this.graphics.x = -(this.width  / 2);
        this.graphics.y = -(this.height / 2);

        this.graphics.clear();
        this.graphics.lineStyle(
            COVER_PARTICLE_EDGE_SIZE,
            gPalette.GetBlockEdgeColor().num(),
            1
        );

        let c = COVER_PARTICLE_EDGE_SIZE / 2;
        if(this.borderEdges[COVER_PARTICLE_EDGE_INDEX_TOP]) {
            this.graphics.moveTo(0,          -c);
            this.graphics.lineTo(this.width, -c);
        }

        if(this.borderEdges[COVER_PARTICLE_EDGE_INDEX_RIGHT]) {
            this.graphics.moveTo(this.width + c, 0);
            this.graphics.lineTo(this.width + c, this.height);
        }

        if(this.borderEdges[COVER_PARTICLE_EDGE_INDEX_BOTTOM]) {
            this.graphics.moveTo(0,          this.height + c);
            this.graphics.lineTo(this.width, this.height + c);
        }

        if(this.borderEdges[COVER_PARTICLE_EDGE_INDEX_LEFT]) {
            this.graphics.moveTo(-c, 0);
            this.graphics.lineTo(-c, this.height);
        }

        this.graphics.endFill();
        this.scale.set(this.targetScale);
    }

    //--------------------------------------------------------------------------
    _CalcRandomOpenForce()
    {
        let x = Random_Int(
            COVER_PARTICLE_ANIM_FORCE_MIN_X,
            COVER_PARTICLE_ANIM_FORCE_MAX_X
        );
        let y = Random_Int(
            COVER_PARTICLE_ANIM_FORCE_MIN_Y,
            COVER_PARTICLE_ANIM_FORCE_MAX_Y
        );

        return Vector_Create(x, y);
    } // _CalcRandomOpenForce

    //--------------------------------------------------------------------------
    _CalcOpenAnimationAngle()
    {
        return Math_Map(
            this.openAnimationForce.x,
            COVER_PARTICLE_ANIM_FORCE_MIN_X,
            COVER_PARTICLE_ANIM_FORCE_MAX_X,
            COVER_PARTICLE_ANIM_ANGLE_MIN,
            COVER_PARTICLE_ANIM_ANGLE_MAX
        );
    } // _CalcOpenAnimationAngle
}; // class CoverParticle
