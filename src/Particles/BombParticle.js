
//----------------------------------------------------------------------------//
// Bomb Particle                                                              //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
const BOMB_PARTICLE_ANIM_REMOVE_TIME = 5.5;

const BOMB_PARTICLE_ANIM_FORCE_MIN_X = -10;
const BOMB_PARTICLE_ANIM_FORCE_MAX_X = +10;

const BOMB_PARTICLE_ANIM_FORCE_MIN_Y = (-120 * 0.9);
const BOMB_PARTICLE_ANIM_FORCE_MAX_Y = (-90  * 0.9);

const BOMB_PARTICLE_ANIM_ANGLE_MIN = -MATH_PI;
const BOMB_PARTICLE_ANIM_ANGLE_MAX = +MATH_PI;

const BOMB_GRAVITY                = 35;
const BOMB_PARTICLE_FORCE_DECAY_Y = 0.9;
const BOMB_PARTICLE_FORCE_DECAY_X = 0.5;

//------------------------------------------------------------------------------
class BombParticle
    extends PIXI.Sprite
{
    //--------------------------------------------------------------------------
    constructor(color)
    {
        super(PIXI.Texture.WHITE);

        //
        // iVars
        // Properties.
        this.rotation    = 0;
        this.targetScale = Vector_Create(1, 0.5);
        this.tint        = color.num();

        this.isAnimating = false;
        this.isDone      = false;

        this.animationTimer = new Base_Timer(0);

        this.removeAnimationForce    = this._CalcRandomRemoveForce   ();
        this.removeAnimationAngle    = this._CalcRemoveAnimationAngle();
        this.removeAnimationVelocity = Vector_Create(0, 0);
    } // ctor

    //--------------------------------------------------------------------------
    SetTargetScale(s)
    {
        this.targetScale = s;
    }

    //--------------------------------------------------------------------------
    StartOpenAnimation()
    {
        this.isAnimating = true;

        this.animationTimer.duration = BOMB_PARTICLE_ANIM_REMOVE_TIME;
        this.animationTimer.Start();
    } // StartAddAnimation

    //--------------------------------------------------------------------------
    Update(dt)
    {
        if(this.isDone || !this.isAnimating) {
            return;
        }

        this.animationTimer.Update(dt);

        if(this.isAdding) {
            this.scale.set(this.animationTimer.ratio * this.targetScale);
        } else if(this.isAnimating) {
            const scale_x = (1 - this.animationTimer.ratio) * this.targetScale.x;
            const scale_y = (1 - this.animationTimer.ratio) * this.targetScale.y;
            this.scale.set(scale_x, scale_y);

            this.removeAnimationForce.x *= BOMB_PARTICLE_FORCE_DECAY_X; // decay...
            this.removeAnimationForce.y *= BOMB_PARTICLE_FORCE_DECAY_Y; // decay...

            let accX =  this.removeAnimationForce.x;
            let accY  = this.removeAnimationForce.y + BOMB_GRAVITY;

            this.removeAnimationVelocity.x += accX;
            this.removeAnimationVelocity.y += accY;

            if(this.removeAnimationVelocity.y >  20) {
                this.removeAnimationVelocity.y = 20;
            }

            this.position.x += this.removeAnimationVelocity.x * dt;
            this.position.y += this.removeAnimationVelocity.y * dt;
            this.rotation   += this.removeAnimationAngle      * dt;

            if(this.animationTimer.isDone) {
                this.isAnimating = false;
                this.isDone     = true;
            }
        }
    } // Update


    //--------------------------------------------------------------------------
    _CalcRandomRemoveForce()
    {
        let x = Random_Int(
            BOMB_PARTICLE_ANIM_FORCE_MIN_X,
            BOMB_PARTICLE_ANIM_FORCE_MAX_X
        );
        let y = Random_Int(
            BOMB_PARTICLE_ANIM_FORCE_MIN_Y,
            BOMB_PARTICLE_ANIM_FORCE_MAX_Y
        );

        return Vector_Create(x, y);
    } // _CalcRandomRemoveForce

     //--------------------------------------------------------------------------
     _CalcRemoveAnimationAngle()
     {
         return Math_Map(
             this.removeAnimationForce.x,
             BOMB_PARTICLE_ANIM_FORCE_MIN_X,
             BOMB_PARTICLE_ANIM_FORCE_MAX_X,
             BOMB_PARTICLE_ANIM_ANGLE_MIN,
             BOMB_PARTICLE_ANIM_ANGLE_MAX
         );
     } // _CalcRemoveAnimationAngle
}; // class BombParticle
